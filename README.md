# INFO3-Unix

Voila, vous avez résolu l'etape A1.

Pour l'étape suivante (A2), la voici, mais elle est encodée en rot13.

Au point où vous en êtes, vous devriez être capables de trouver (en
quelques secondes) ce qu'est rot13, et un moyen de le décoder
facilement.

Ibvyn, yr ebg13 rfg qrpbqr.

Fv ibhf rgrf znyvaf, ibhf nirm fnaf qbhgr erznedhr dhr yr jro ertbetr
qr pbqrhe/qrpbqrhe ebg13 ra yvtar, qbap, ha oba zbgrhe qr erpurepur n
yn znva, ba gebhir encvqrzrag fba obaurhe (rg p'rgnvg ovra yr ohg qr
y'rkrepvpr !). Yrf Rznpfvraf nhebag cersrer Z-k ebg13-ertvba ERG, znvf
p'rfg har nhger uvfgbver.

Abhf dhvggbaf znvagranag yr jvxv NVE cbhe Zbbqyr. Zbbqyr rfg npprffvoyr
qrchvf yr jvxv qr y'VZ2NT (uggcf://vz2nt-jvxv.hws-teraboyr.se/).
Nyyrm-l, rg gebhirm yr pbhef Cbylgrpu « Havk » ra VASB3 (rk-EVPZ3).
Yn pyr fren 'wqc'.
Ibhf gebhirerm ha qbphzrag dhv rfg y'égncr N3 qh wrh qr cvfgr.
